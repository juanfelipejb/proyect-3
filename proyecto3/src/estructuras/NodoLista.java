package estructuras;

public class NodoLista <T>
{
	T elemento;
	NodoLista<T> anterior;
	NodoLista<T> siguiente;

	public NodoLista(T element)
	{
		elemento = element;
		anterior = null;
		siguiente = null;
	}

	public T getElemento() 
	{
		return elemento;
	}

	public void setElemento(T elemento) 
	{
		this.elemento = elemento;
	}

	public NodoLista<T> getAnterior() 
	{
		return anterior;
	}

	public void setAnterior(NodoLista<T> anterior) 
	{
		this.anterior = anterior;
	}

	public NodoLista<T> getSiguiente() 
	{
		return siguiente;
	}

	public void setSiguiente(NodoLista<T> siguiente) 
	{
		this.siguiente = siguiente;
	}


}
