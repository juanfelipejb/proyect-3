package estructuras;

import java.util.Iterator;

import estructuras.NodoLista;

public class Lista <T> implements Iterable<T>
{
	NodoLista<T> primero;
	NodoLista<T> ultimo;
	int tamanio;

	public Lista()
	{
		primero = null;
		ultimo=null;
		tamanio = 0;
	}

	public NodoLista<T> getPrimero() 
	{
		return primero;
	}

	public NodoLista<T> getUltimo() 
	{
		return ultimo;
	}

	public int darTamanio() 
	{
		return tamanio;
	}

	public boolean isEmpty()
	{
		if(primero==null)
			return true;
		return false;
	}

	public void agregarPrimero(T elem)
	{
		if(isEmpty())
		{
			primero = new NodoLista<T>(elem);
			ultimo = primero;
			tamanio ++;
		}
		else
		{
			NodoLista<T> nuevo = new NodoLista<T>(elem);
			nuevo.setSiguiente(primero);
			primero.setAnterior(nuevo);
			primero=nuevo;
			tamanio ++;
		}
	}

	public void agregarUltimo(T elemento)
	{
		if(isEmpty())
		{
			primero = new NodoLista<T>(elemento);
			ultimo = primero;
			tamanio ++;
		}
		else
		{
			NodoLista<T> nuevo = new NodoLista<T>(elemento);
			ultimo.setSiguiente(nuevo);
			nuevo.setAnterior(ultimo);
			ultimo = nuevo;
			tamanio ++;
		}
	}

	public void eliminarPrimero()
	{
		if(!isEmpty())
		{
			if(primero.getSiguiente()!=null)
			{
				primero=primero.getSiguiente();
				primero.setAnterior(null);
				tamanio --;
			}
			else
			{
				primero=null;
				ultimo=null;
				tamanio --;
			}
		}
	}

	public void eliminarUltimo()
	{
		if(!isEmpty())
		{
			if(ultimo==primero)
			{
				primero=null;
				ultimo=null;
				tamanio --;
			}
			else
			{
				ultimo = ultimo.getAnterior();
				ultimo.setSiguiente(null);
				tamanio --;
			}
		}
	}

	public void eliminar(T elemento)
	{
		if(!isEmpty())
		{
			if(primero.getElemento()==elemento)
			{
				if(primero==ultimo)
				{
					primero = ultimo = null;
					tamanio --;
				}
				else
				{
					primero = primero.getSiguiente();
					primero.setAnterior(null);
					tamanio --;
				}
			}
			else if (ultimo.getElemento()==elemento)
			{
				if(primero==ultimo)
				{
					primero = ultimo = null;
					tamanio --;
				}
				else
				{
					ultimo = ultimo.getAnterior();
					ultimo.setSiguiente(null);
					tamanio --;
				}
			}
			else
			{
				NodoLista<T> aEliminar = buscar(elemento);
				if(aEliminar!=null){
					aEliminar.getAnterior().setSiguiente(aEliminar.getSiguiente());
					aEliminar.getSiguiente().setAnterior(aEliminar.getAnterior());
					aEliminar.setSiguiente(null);
					aEliminar.setAnterior(null);
				}
			}
		}
	}

	public T eliminarT(T elemento)
	{
		T eliminado= null;
		if(!isEmpty())
		{
			if(primero.getElemento()==elemento)
			{
				if(primero==ultimo)
				{
					eliminado= primero.getElemento();
					primero = ultimo = null;
					tamanio --;
				}
				else
				{
					eliminado= primero.getElemento();
					primero = primero.getSiguiente();
					primero.setAnterior(null);
					tamanio --;
				}
			}
			else if (ultimo.getElemento()==elemento)
			{
				if(primero==ultimo)
				{
					eliminado= ultimo.getElemento();
					primero = ultimo = null;
					tamanio --;
				}
				else
				{
					eliminado= ultimo.getElemento();
					ultimo = ultimo.getAnterior();
					ultimo.setSiguiente(null);
					tamanio --;
				}
			}
			else
			{
				NodoLista<T> aEliminar = buscar(elemento);
				if(aEliminar!=null){
					eliminado= aEliminar.getElemento();
					aEliminar.getAnterior().setSiguiente(aEliminar.getSiguiente());
					aEliminar.getSiguiente().setAnterior(aEliminar.getAnterior());
					aEliminar.setSiguiente(null);
					aEliminar.setAnterior(null);
				}
			}
		}
		return eliminado;
	}


	public NodoLista<T> buscar(T elemento)
	{
		boolean encontro = false;
		NodoLista<T> actual = primero;
		NodoLista<T> buscado = null;
		while(!encontro&&actual!=null)
		{
			if(actual.getElemento()==elemento)
			{
				buscado = actual;
			}
			actual = actual.getSiguiente();
		}
		return buscado;
	}

	public T buscarT(T elemento)
	{
		boolean encontro = false;
		NodoLista<T> actual = primero;
		T buscado = null;
		while(!encontro&&actual!=null)
		{
			if(actual.getElemento()== elemento)
			{
				buscado= actual.getElemento();
			}
			actual = actual.getSiguiente();
		}
		return buscado;
	}

	public T get(int pos)
	{
		T get= null;

		NodoLista<T> actual= primero;

		for(int i=0; i< darTamanio(); i++){

			if(i==pos){
				get=  actual.getElemento();
			}
			actual= actual.getSiguiente();
		}
		return get;
	}

	@Override
	public Iterator<T> iterator() 
	{
		return new ListaIterator();
	}

	public void vaciar()
	{
		primero= null;
		ultimo= null;
		tamanio= 0;
	}

	public Iterador<T> darIterador( )
	{
		IteradorSimple<T> respuesta = new IteradorSimple<T>(tamanio);
		for( int i = 0; i < tamanio; i++ )
		{
			try
			{
				respuesta.agregar( get(i));
			}
			catch(Exception e )
			{
				// Nunca debería ocurrir esta excepción
			}
		}
		return respuesta;
	}

	private class ListaIterator implements Iterator<T>
	{

		NodoLista<T> actual = primero;
		@Override
		public boolean hasNext() 
		{
			return actual!=null;
		}

		@Override
		public T next() 
		{
			T retorno = actual.getElemento();
			actual = actual.getSiguiente();
			return retorno;
		}

		@Override
		public void remove()
		{

		}

	}

}
