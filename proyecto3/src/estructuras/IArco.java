package estructuras;

import java.io.Serializable;

/**
* Interfaz utilizada para representar las responsabilidades mínimas de un arco
*/
public interface IArco extends Serializable
{
// -----------------------------------------------------------------
// Métodos
// -----------------------------------------------------------------

/**
* Devuelve el peso del arco
* @return Peso del arco
*/
public int darPeso( );
}