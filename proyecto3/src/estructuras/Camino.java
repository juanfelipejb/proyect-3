package estructuras;

/**
 * Representa un camino en un grafo
 * @param <K> Tipo del identificador de un vértice
 * @param <V> Tipo de datos del elemento del vértice
 * @param <A> Tipo de datos del elemento del arco
 */
public class Camino<K, V extends IVertice<K>, A extends IArco>
{
	// -----------------------------------------------------------------
	// Atributos
	// -----------------------------------------------------------------

	/**
	 * Lista con los arcos del camino
	 */
	private Lista<A> arcos;

	/**
	 * Lista con los vértices del camino
	 */
	private Lista<V> vertices;

	/**
	 * Origen del camino
	 */
	private V origen;

	/**
	 * Costo total del camino
	 */
	private int costo;

	// -----------------------------------------------------------------
	// Constructor
	// -----------------------------------------------------------------

	/**
	 * Constructor del camino
	 * @param origen Vértice de origen del camino
	 */
	public Camino( V origen )
	{
		// Inicializar los atributos del camino
		vertices = new Lista<V>( );
		arcos = new Lista<A>( );
		costo = 0;
		this.origen = origen;
	}

	// -----------------------------------------------------------------
	// Métodos
	// -----------------------------------------------------------------

	/**
	 * Agrega un arco al final del camino
	 * @param arco Arco a agregar
	 */
	public void agregarArcoFinal( V vertice, A arco )
	{
		arcos.agregarUltimo( arco );
		vertices.agregarUltimo( vertice );
		costo += arco.darPeso( );
	}

	/**
	 * Agrega un arco al comienzo del camino.
	 * @param nuevoOrigen Nuevo origen del camino
	 * @param arco Arco que va del nuevo origen al antiguo origen del camino
	 */
	public void agregarArcoComienzo( V nuevoOrigen, A arco )
	{
		arcos.agregarPrimero( arco);
		vertices.agregarPrimero( origen);
		origen = nuevoOrigen;
		costo += arco.darPeso( );
	}

	/**
	 * Concatena todos los arcos del camino especificado al final del camino
	 * @param camino Camino a concatenar
	 */
	public void concatenar( Camino<K, V, A> camino )
	{
		// Agregar los arcos y vertices del camino a concatenar ignorando el origen del camino ingresado por parámetro
		for( int i = 0; i < camino.arcos.darTamanio(); i++ )
			agregarArcoFinal( camino.vertices.get( i ), camino.arcos.get( i ) );
	}

	/**
	 * Elimina el último arco
	 */
	public void eliminarUltimoArco( )
	{
		if( arcos.darTamanio() >= 1 )
		{
			A arco = arcos.get( arcos.darTamanio() - 1 );
			arcos.eliminar( arco);
			vertices.eliminar( vertices.get(vertices.darTamanio()-1) );
			costo -= arco.darPeso( );
		}
	}

	/**
	 * Reinicia el camino conservando el origen
	 */
	public void reiniciar( )
	{
		arcos.vaciar( );
		vertices.vaciar( );
		costo = 0;
	}

	/**
	 * Devuelve la longitud del camino
	 * @return Longitud del camino
	 */
	public int darLongitud( )
	{
		return arcos.darTamanio();
	}

	/**
	 * Devuelve el costo del camino
	 * @return Costo del camino
	 */
	public int darCosto( )
	{
		return costo;
	}

	/**
	 * Devuelve los vértices por los cuales pasa el camino
	 * @return Iterador sobre los vértices
	 */
	public Iterador<V> darSecuenciaVertices( )
	{
		// Crear una lista auxiliar y agregarle el origen
		Lista<V> aux = new Lista<V>( );
		aux.agregarPrimero( origen );

		// Poblara la lista auxiliar con los vértices del camino
		for( int i = 0; i < vertices.darTamanio(); i++ )
		{
			aux.agregarPrimero( vertices.get( i ) );
		}

		// Retornar el iterador
		return aux.darIterador( );
	}

	/**
	 * Retorna el origen del camino
	 * @return El vertice desde el que se origina el camino
	 */
	public V darOrigen( )
	{
		return origen;
	}
}