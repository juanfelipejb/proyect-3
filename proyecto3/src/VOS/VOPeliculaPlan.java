package VOS;

import java.sql.Date;
import java.sql.Time;
import java.util.Calendar;

/**
 *  
 * @Author: Tomas F. Venegas 
 */
public class VOPeliculaPlan {
	
	/**
	 * Atributo que apunta a la pelicula que se propone
	 */
	private VOPelicula pelicula;
	
	
	/**
	 * Atributo que apunta al teatro que se propone
	 */
	private VOTeatro teatro;
	
	/**
	 * Atributo que modela la hora de inicio de la pelicula
	 */
	private Time horaInicio;
	
	/**
	 * Atributo que modela la hora de fin de la pelicula
	 */
	private Time horaFin;
	
	/**
	 * Atributo que modela el dia de presentacion de la pelicla
	 * (1..5)
	 */
	
	private int dia;
	
	private String franja;
	
	public VOPeliculaPlan() {
		//constructor automaticamente crea la hora de fin dos horas despues de la hora inicial
				Calendar cal = Calendar.getInstance(); // creates calendar
			    cal.setTime(horaInicio); // sets calendar time/date
			    cal.add(Calendar.HOUR_OF_DAY, 2); // mas 2 horas
			   this.horaFin = (Time) cal.getTime(); // returns new date object, 2 hours in the future
			   //constuctor crea automaticamente la franja de este plan, para eso creamos 3 objetos que seran el rango
			   Calendar calMa = Calendar.getInstance();
			   calMa.set(Calendar.HOUR_OF_DAY,7); // creacion de la manana
			   Time manana = (Time) calMa.getTime();
			   
			   Calendar calTar = Calendar.getInstance();
			   calTar.set(Calendar.HOUR_OF_DAY,12);// creacion de la tarde
			   Time tarde = (Time) calTar.getTime();
			   
			   Calendar calNo = Calendar.getInstance();
			   calNo.set(Calendar.HOUR_OF_DAY,18);
			   Time noche = (Time) calNo.getTime();
				if(horaInicio.after(manana) && horaInicio.before(tarde))
				{
					this.setFranja("manana");
				}
				if(horaInicio.after(tarde) && horaInicio.before(noche)){
					this.setFranja("tarde");
				}
				if(horaInicio.after(noche)){
					this.setFranja("noche");
				}
	}

	public VOPelicula getPelicula() {
		return pelicula;
	}

	public void setPelicula(VOPelicula pelicula) {
		this.pelicula = pelicula;
	}

	public VOTeatro getTeatro() {
		return teatro;
	}

	public void setTeatro(VOTeatro teatro) {
		this.teatro = teatro;
	}

	public Time getHoraInicio() {
		return horaInicio;
	}

	public void setHoraInicio(Time horaInicio) {
		this.horaInicio = horaInicio;
	}

	public Time getHoraFin() {
		return horaFin;
	}

	public void setHoraFin(Time horaFin) {
		this.horaFin = horaFin;
	}

	public int getDia() {
		return dia;
	}

	public void setDia(int dia) {
		this.dia = dia;
	}

	public String getFranja() {
		return franja;
	}

	public void setFranja(String franja) {
		this.franja = franja;
	}
	
	
	

}
