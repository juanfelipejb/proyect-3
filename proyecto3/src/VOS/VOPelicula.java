package VOS;

import estructuras.MaxPQ;

public class VOPelicula {
	
	private int id;
	private String titulo;
	private String[] generos;
	private MaxPQ<VOSimilitud> similitudes;
	
	
	
	public VOPelicula(int id, String titulo, String[] generos) 
	{
		this.id = id;
		this.titulo = titulo;
		this.generos = generos;
	}
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String[] getGeneros() {
		return generos;
	}
	public void setGeneros(String[] generos) {
		this.generos = generos;
	}


	public MaxPQ<VOSimilitud> getSimilitudes() {
		return similitudes;
	}


	public void setSimilitudes(MaxPQ<VOSimilitud> similitudes) {
		this.similitudes = similitudes;
	}
	
	

}
