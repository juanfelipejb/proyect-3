package VOS;

import estructuras.RedBlackBST;

/**
 *  
 * @Author: Tomas F. Venegas 
 */
public class VOUsuario 

{

	/**
	 * Atributo que modela el id del usuario
	 */

	private int id;
	
	private boolean registrado;
	
	private RedBlackBST<Integer,VOPelicula> peliculas;
	
	public VOUsuario() 
	{
		peliculas= new RedBlackBST<>();
		// TODO Auto-generated constructor stub
	}

	public Integer getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isRegistrado() {
		return registrado;
	}

	public void setRegistrado(boolean registrado) {
		this.registrado = registrado;
	}

	public RedBlackBST<Integer,VOPelicula> getPeliculas() {
		return peliculas;
	}

	public void setPeliculas(RedBlackBST<Integer,VOPelicula> peliculasCalificadas) {
		this.peliculas = peliculasCalificadas;
	}
	
	
}
