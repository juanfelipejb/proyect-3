package VOS;

import javax.swing.text.html.HTMLDocument.Iterator;

public class VOSimilitud implements Comparable<VOSimilitud>
{
	private double similitud;
	private int id;

	public VOSimilitud(double s, int i) 
	{
		similitud= s;
		id= i;
	}

	public double getSimilitud()
	{
		return similitud;
	}

	public int getId()
	{
		return id;
	}

	@Override
	public int compareTo(VOSimilitud o) 
	{
		if(this.getSimilitud() > o.getSimilitud())
		{
			return 1;
		}
		else if(this.getSimilitud() < o.getSimilitud())
		{
			return -1;
		}
		else
		{
			return 0;
		}
	}

}
