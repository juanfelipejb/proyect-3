
package VOS;

import java.util.Iterator;

import estructuras.IArco;
import estructuras.IVertice;
import estructuras.RedBlackBST;

/**
 *  
 * @Author: Tomas F. Venegas 
 */
public class VOTeatro implements IVertice<String>{
	
	/**
	 * Atributo que modela el nombre del teatro
	 */
	
	private String nombre;
	
	/**
	 * Atributo que modela la ubicacion del teatro 
	 */
	
	private VOUbicacion ubicacion;
	
	/**
	 * Atributo que referencia la franquicia
	 */
	
	private VOFranquicia franquicia;
	
	private RedBlackBST<Integer, RedBlackBST<Integer, VOPeliculaPlan>> funciones;

	
	
	public VOTeatro() 
	{
		funciones= new RedBlackBST<Integer, RedBlackBST<Integer, VOPeliculaPlan>>();
		Iterator<Integer> i=funciones.keys().iterator();
		while(i.hasNext())
		{
			int dia=i.next();
			RedBlackBST s=funciones.get(dia);
			s=new RedBlackBST<>();
		}
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public VOUbicacion getUbicacion() {
		return ubicacion;
	}


	public void setUbicacion(VOUbicacion ubicacion) {
		this.ubicacion = ubicacion;
	}


	public VOFranquicia getFranquicia() {
		return franquicia;
	}


	public void setFranquicia(VOFranquicia franquicia) {
		this.franquicia = franquicia;
	}


	@Override
	public String darId() {
		return nombre;
	}

	public RedBlackBST<Integer, RedBlackBST<Integer, VOPeliculaPlan>> getFunciones()
	{
		return funciones;
	}
	
	
}
