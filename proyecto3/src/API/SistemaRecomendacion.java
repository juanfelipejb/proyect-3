package API;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Time;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;

import estructuras.CaminosMinimos;
import estructuras.GrafoDirigido;
import estructuras.IVertice;
import estructuras.Lista;
import estructuras.MaxPQ;
import estructuras.MinPQ;
import estructuras.NodoLista;
import estructuras.RedBlackBST;
import estructuras.SeparateChainingHashST;
import estructuras.Vertice;
import VOS.VOArco;
import VOS.VOFranquicia;
import VOS.VOGeneroPelicula;
import VOS.VOPelicula;
import VOS.VOPeliculaPlan;
import VOS.VOTeatro;
import VOS.VOUbicacion;
import VOS.VOUsuario;
import VOS.VOSimilitud;

public class SistemaRecomendacion implements ISistemaRecomendacion
{

	private static RedBlackBST<Integer, VOPelicula> peliculas;
	private static GrafoDirigido<String, VOTeatro, VOArco> grafoTiempo;
	private static GrafoDirigido<String, VOTeatro, VOArco> grafoDistancia;
	private static SeparateChainingHashST<String, VOFranquicia> franquicias;
	private static SeparateChainingHashST<Integer, VOUsuario> usuarios;
	public static int DIA;

	@Override
	public ISistemaRecomendacion crearSR() {
		usuarios= new SeparateChainingHashST<>();
		peliculas= new RedBlackBST<>();
		grafoTiempo= new GrafoDirigido<>();
		grafoDistancia= new GrafoDirigido<>();
		franquicias= new SeparateChainingHashST<>();
		return this;
	}

	@Override
	public boolean cargarTeatros(String ruta) 
	{
		try 
		{
			FileReader teatros = new FileReader(new File(ruta));
			JsonReader reader = new JsonReader(teatros);
			handleObjectT(reader);
			return true;
		} 
		catch (Exception e) 
		{
			System.out.println(e.getMessage());
			return false;
		}
	}

	@Override
	public boolean cargarCartelera(String ruta) 
	{
		try 
		{
			if(ruta.contains("1"))
			{
				DIA= 1;
			}
			else if(ruta.contains("2"))
			{
				DIA=2;
			}
			else if(ruta.contains("3"))
			{
				DIA=3;
			}
			else if(ruta.contains("4"))
			{
				DIA=4;
			}
			else if(ruta.contains("5"))
			{
				DIA=5;
			}
			FileReader cartelera = new FileReader(new File(ruta));
			JsonReader reader = new JsonReader(cartelera);
			handleObjectC(reader);	
			return true;
		} 
		catch (Exception e) 
		{
			System.out.println(e.getMessage());
			return false;
		}
	}

	@Override
	public boolean cargarRed(String ruta) 
	{
		try {
			FileReader redes = new FileReader(new File(ruta));
			JsonReader reader = new JsonReader(redes);
			handleObjectR(reader);	
			return true;
		} 
		catch (Exception e) 
		{
			System.out.println(e.getMessage());
			return false;
		}
	}

	public boolean cargarPeliculas(String rutaSimilitudes, String rutaMovies) 
	{
		try {
			FileReader peliculas = new FileReader(new File(rutaMovies));
			JsonReader readerM = new JsonReader(peliculas);
			FileReader similitudes = new FileReader(new File(rutaSimilitudes));
			JsonReader readerS = new JsonReader(similitudes);
			handleObjectM(readerM);
			handleObjectS(readerS);
			return true;
		} 
		catch (Exception e) 
		{
			System.out.println(e.getMessage());
			return false;
		}
	}


	//CORREGIR, NO LEE SINO SOLAMENTE 3 ARCOS Y PARA,
	//TIENE QUE VER CON AGREGARLO AL GRAFO
	//	PORQUE SI SE OMITE ESA PARTE EN EL CÃ“DIGO, LEE NORMAL TODAS LAS DISTANCIAS
	public void leerDistancias(String ruta)
	{
		String csv = ruta;
		BufferedReader br = null;
		String linea = "";
		String separador = ",";
		try {
			br = new BufferedReader(new FileReader(csv));
			linea = br.readLine();

			while ((linea=br.readLine()) != null ) 
			{
				String[] doc = linea.split(separador);

				String teatro1 = doc[0];
				String teatro2 = doc[1];
				String distancia = doc[2];
				//OJO ACA CON LA DISTANCIA, PUES EN EL ARCHIVO ESTAN EN KM Y ESTAN SEPARADOS POR PUNTO
				//				ENTONCES SI HAY UN 2.3583 ES 2.3 KM, PERO SI SE QUIERE VOLVER DOUBLE (PORQUE PARA QUE FUNCIONE TOCA CON UN INT) ESO CAMBIA MUCHO TODO
				//				ENTONCES TOCARÃ� SABERLO COMO MANEJAR.
				int dist= Integer.parseInt(distancia.replaceAll("\\.", ""));

				//POR ACA DEBE SER EL ERROR, SI SE OMITEN LAS SIGUIENTES DOS LINEAS EL CODIGO FUNCIONA PERFECTAMENTE AL LEER
				VOArco ar= new VOArco(dist);
				grafoDistancia.agregarArco(teatro1, teatro2, ar);


			}
		}
		catch(Exception e)
		{
			e.getMessage();
		}
	}

	@Override
	public int sizeMovies() 
	{
		return peliculas.size();
	}

	@Override
	public int sizeTeatros() 
	{
		return grafoTiempo.darVertices().darTamanio();
	}

	@Override
	public Lista<VOPeliculaPlan> PlanPeliculas(VOUsuario usuario, int fecha) throws Exception 
	{
		//creamos lista de planes
		Lista<VOPeliculaPlan> returnList = new Lista<VOPeliculaPlan>();
		// obtiene el usario
		VOUsuario user = usuarios.get(usuario.getId());
		if(user==null)
		{
			throw new Exception ("No existe el usuario ingresado.");
		}
		//obtiene lista de peliculas, las pone en un maxHeap,
			RedBlackBST<Integer,VOPelicula> arbol = user.getPeliculas();
			MaxPQ<Integer> maxpq = new MaxPQ(arbol.size());
		Iterator<Integer> iter = arbol.keys().iterator();
		while(iter.hasNext())
		{
			maxpq.insert(iter.next());
		}	
		//obtenemos vertices, es decir los teatros
		Lista<VOTeatro> teatros = grafoTiempo.darVertices();
		
		//Creacion de objetos afuera del for para no crear tantos objetos
		VOTeatro teatro = new VOTeatro();
		VOPeliculaPlan plan = new VOPeliculaPlan();

		VOPelicula peliculaFav=null;
		VOPeliculaPlan planAnterior = null;
		RedBlackBST<Integer, RedBlackBST<Integer, VOPeliculaPlan>> dias= null;
		RedBlackBST<Integer, VOPeliculaPlan> planesPeliculasTeatro= null;
		System.out.println("vacia: "+maxpq.isEmpty());
		int i=0;
		// for para crear la lista de planes,un usario puede ver maximo 4 peliculas, 
		while(returnList.darTamanio() < 5 || !maxpq.isEmpty()){
			System.out.println("Tamaño lista: "+returnList.darTamanio());
		//Obtiene la primera pelicula del heap
		 peliculaFav = peliculas.get(maxpq.delMax());
		//Iteramos por la lista de teatros, buscando la primera del heap,
		
		 Iterator<VOTeatro> iterTeatros= teatros.iterator();

		while(iterTeatros.hasNext()){
			teatro = iterTeatros.next();
			//luego iteramos por la lista de planpelicula del dia respectivo
			 dias = teatro.getFunciones();
			 planesPeliculasTeatro = dias.get(fecha);
			 
			 System.out.println("***** "+planesPeliculasTeatro.size());
				Iterator<Integer> iterPlanes=planesPeliculasTeatro.keys().iterator();
					while(iterPlanes.hasNext()){
						plan = planesPeliculasTeatro.get(iterPlanes.next());
						//en caso de ser la primera pelicula en la lista
						if(plan.getPelicula().getTitulo().equals(peliculaFav.getTitulo()))
						{
							returnList.agregarUltimo(plan);
//							continue;
						}
						if(plan.getPelicula().equals(peliculaFav)){
						planAnterior = returnList.getUltimo().getElemento();
						}
						if(plan.getHoraInicio().after(planAnterior.getHoraFin()))
							{
						returnList.agregarUltimo(plan);
							}
						}	
					}
				}	
		return returnList;
	}

	@Override
	public Lista<VOPeliculaPlan> PlanPorGenero(VOGeneroPelicula genero, VOUsuario usuario) 
	{
		//creamos lista de planes
		
		Lista<VOPeliculaPlan> returnList = new Lista<VOPeliculaPlan>();
		// obtiene el usario
		VOUsuario user = usuarios.get(usuario.getId());
		
		//obtenemos vertices, es decir los teatros
				Lista<VOTeatro> teatros = grafoTiempo.darVertices();
				
		//objetos creados afuera del while		
		VOPeliculaPlan plan= null;
		VOPeliculaPlan planAnterior = null;
		VOPelicula pelicula= null;
		RedBlackBST<Integer, RedBlackBST<Integer, VOPeliculaPlan>> dias= null;
		RedBlackBST<Integer, VOPeliculaPlan> planesPeliculasTeatro= null;
		MaxPQ<Lista<VOPeliculaPlan>> agendaPorDia = new MaxPQ(5);//comparable
//		Lista agenda=null;;
		for(int i =0;i<5;i++){
			Iterator<VOTeatro> iterTeatros = teatros.iterator();
		while(iterTeatros.hasNext()){
			VOTeatro teatro = (VOTeatro) iterTeatros.next();	
			dias = teatro.getFunciones();
			planesPeliculasTeatro = dias.get(i);
			Iterator<Integer> iterPlanes = planesPeliculasTeatro.keys().iterator();
			while(iterPlanes.hasNext()){
				plan = planesPeliculasTeatro.get(iterPlanes.next());
				pelicula = plan.getPelicula();
				boolean a=false;
				String[] lista = pelicula.getGeneros();
				for (int j = 0; j < lista.length; j++) {
					if(lista[j].equals(genero))
					{
						a=true;
					}
				}
				if(a && plan.getHoraInicio().after(planAnterior.getHoraFin())){
					returnList.agregarUltimo(plan);
					}
				}
			}
		agendaPorDia.insert(returnList);
		}
		return agendaPorDia.delMax();
	}

	@Override
	public Lista<VOPeliculaPlan> PlanPorFranquicia(VOFranquicia franquicia, int fecha, String franja) 
	{
		Lista<VOPeliculaPlan> returnList = new Lista<VOPeliculaPlan>();
		//objetos creados afuera del while		
		VOPeliculaPlan plan=null;
		VOPeliculaPlan planAnterior=null;
		VOPelicula pelicula=null;
		VOTeatro teatro=null;
		RedBlackBST<Integer, RedBlackBST<Integer, VOPeliculaPlan>> dias= null;
		RedBlackBST<Integer, VOPeliculaPlan> planesPeliculasTeatro= null;
		Lista<VOTeatro> teatros = grafoTiempo.darVertices();
		Iterator<VOTeatro> iterTeatros = teatros.iterator();
		while(iterTeatros.hasNext()){
			teatro = (VOTeatro) iterTeatros.next();
			if(teatro.getFranquicia().equals(franquicia))
			{
			dias = teatro.getFunciones();
			planesPeliculasTeatro = dias.get(fecha);
			Iterator<Integer> iterPlanes = planesPeliculasTeatro.keys().iterator();
			
			while(iterPlanes.hasNext())
			{
				plan =  (VOPeliculaPlan) planesPeliculasTeatro.get(iterPlanes.next());
				planAnterior =(VOPeliculaPlan)returnList.getUltimo().getElemento();	
				//en caso de ser la primera pelicula en la lista
				if(returnList.isEmpty() && plan.getFranja().equals(franja))
				{
					returnList.agregarUltimo(plan);
					continue;
				}
						
					 if(plan.getFranja().equals(franja) &&  plan.getHoraInicio().after(planAnterior.getHoraFin()))
							{
						returnList.agregarUltimo(plan);
							}
				}
			
			}
		}
		return returnList;
	}

	@Override
	public ILista<VOPeliculaPlan> PlanPorGeneroYDesplazamiento(VOGeneroPelicula genero, int fecha) 
	{
		Vertice vertice;
		Vertice origen;
		VOTeatro teatro;
		RedBlackBST dias;
		RedBlackBST planes;
		VOPeliculaPlan plan;
		VOPeliculaPlan planAnterior;
		Lista vertices = grafoTiempo.darArcos();
		teatro = (VOTeatro) vertices.getPrimero().getElemento();
		CaminosMinimos camino = grafoTiempo.dijkstra(teatro.darId());
		Lista<VOPeliculaPlan> returnList = new Lista();
		String[] generos;
		HashMap nodos = camino.getNodos();
		
		Iterator<Integer> iterNodos = nodos.keySet().iterator();
		RedBlackBST funciones = (RedBlackBST) dias.get(fecha);
			while(returnList.darTamanio() < 5 && iterNodos.hasNext()){	
				vertice = (Vertice) nodos.get(iterNodos.next()); 
				teatro = vertice.darElemento();
				dias= teatro.getFunciones();
				planes = (RedBlackBST) dias.get(fecha);
				Iterator<Integer> iterPlanes = planes.keys().iterator();
				while(iterPlanes.hasNext()){
					boolean a=false;
					plan = (VOPeliculaPlan) planes.get(iterPlanes.next());
					generos = plan.getPelicula().getGeneros();
					for (int i = 0; i < generos.length; i++) 
					{
						if(generos[i].equals(genero))
						{
							a=true;
						}
					}
						if(returnList.isEmpty() && a){
							returnList.agregarUltimo(plan);
							continue;
						}
						planAnterior = returnList.getUltimo().getElemento();
					if(a &&  plan.getHoraInicio().after(planAnterior.getHoraFin()))
						returnList.agregarUltimo(plan);
					
					
				}
				      
			}
	}

	@Override
	public ILista<VOPeliculaPlan> PlanPorGeneroDesplazamientoYFranquicia(VOGeneroPelicula genero, int fecha,
			VOFranquicia franquicia) throws Exception 
	{
		VOTeatro teatro;
		RedBlackBST dias = null;
		RedBlackBST planes;
		VOPeliculaPlan plan;
		VOPeliculaPlan planAnterior;
		Lista vertices = grafoTiempo.darArcos();
		teatro = (VOTeatro) vertices.getPrimero().getElemento();
		CaminosMinimos camino = grafoTiempo.dijkstra(teatro.darId());
		Lista<VOPeliculaPlan> returnList = new Lista();
		String[] generos;
		HashMap nodos = camino.getNodos();
		
		Iterator<Integer> iterNodos = nodos.keySet().iterator();
		RedBlackBST funciones = (RedBlackBST) dias.get(fecha);
			while(returnList.darTamanio()> 4 && iterNodos.hasNext()){	
				Vertice vertice = (Vertice) nodos.get(iterNodos.next()); 
			
				
				dias= teatro.getFunciones();
				planes = (RedBlackBST) dias.get(fecha);
				Iterator<Integer> iterPlanes = planes.keys().iterator();
				while(iterPlanes.hasNext()){
					boolean a=false;
					plan = (VOPeliculaPlan) planes.get(iterPlanes.next());
					generos = plan.getPelicula().getGeneros();
					for (int i = 0; i < generos.length; i++) 
					{
						if(generos[i].equals(genero))
						{
							a=true;
						}
					}
						if(returnList.isEmpty() && a){
							returnList.agregarUltimo(plan);
							continue;
						}
						planAnterior = returnList.getUltimo().getElemento();
					if(a &&  plan.getHoraInicio().after(planAnterior.getHoraFin()))
						returnList.agregarUltimo(plan);
					
					
				}
				      
			}
			return (ILista<VOPeliculaPlan>) returnList;
	}

	@Override
	public ILista<IEdge<VOTeatro>> generarMapa() 
	{
		Lista<VOArco> arcos = grafoTiempo.darArcos();
		MinPQ<VOArco> arcosMinimos = new MinPQ(arcos.darTamanio());
		Lista arbolMinimo =new Lista();
		Iterator iter = (Iterator) arcos.darIterador();
		GrafoDirigido<String,VOTeatro,VOArco> MST = new GrafoDirigido<>();
		while(iter.hasNext()){
			arcosMinimos.insert((VOArco) iter.next());
		}
		 while (!arcosMinimos.isEmpty() && arbolMinimo.darTamanio() < MST.darArcos().darTamanio() - 1){
		VOArco arco = arcosMinimos.delMin();
//		MST.agregarArco(arco.daridVerticeOrigen(), arco.idVerticeDestino, arco.darInfoArco);
		if(MST.esAciclico()){
			arbolMinimo.agregarUltimo(arco);
		}
		else{
//			MST.eliminarArco(arco.daridVerticeOrigen(),arco.idVerticeDestino);
		}
		return (ILista<IEdge<VOTeatro>>) arbolMinimo;
	}
		
		
		
		return null;
		// TODO Auto-generated method stub
	}

	@Override
	public ILista<ILista<VOTeatro>> rutasPosible(VOTeatro origen, int n) 
	{

		// TODO Auto-generated method stub
		return null;
	}

	private static void handleObjectT(JsonReader reader) throws Exception
	{
		reader.beginArray();
		while (reader.hasNext()) 
		{

			JsonToken token = reader.peek();
			if (token.equals(JsonToken.BEGIN_OBJECT)) //
				handleArrayT(reader);
			else if (token.equals(JsonToken.END_OBJECT)) 
			{
				reader.endObject();
				return;
			}
		}
	}

	private static void handleArrayT(JsonReader reader) throws Exception
	{		
		String nombre="";
		String ubicacion="";
		String franquicia="";
		String frTemp="";
		reader.beginObject();
		boolean a=false;
		while (true) {
			JsonToken token = reader.peek();
			if(token.equals(JsonToken.END_OBJECT))
				reader.endObject();
			if (token.equals(JsonToken.END_ARRAY)) 
			{
				return;
			} 
			else if (token.equals(JsonToken.BEGIN_OBJECT)) 
			{
				handleArrayT(reader);
			} 
			else if(token.equals(JsonToken.NAME)) 
			{

				String res= reader.nextName();
				if(res.equals("Nombre"))
				{
					nombre= reader.nextString();
				}
				else if(res.equals("Ubicacion geografica (Lat|Long)"))
				{
					ubicacion= reader.nextString();
				}
				else if( res.equals("Franquicia"))
				{
					franquicia= reader.nextString();
					a=true;
				}	
			}

			if(a)
			{
				a=false;

				if(!franquicias.contains(franquicia))
				{
					VOFranquicia f=new VOFranquicia();
					f.setNombre(franquicia);
					franquicias.put(franquicia, f);
				}

				VOUbicacion u= new VOUbicacion();
				String[] ll=ubicacion.split("\\|");
				u.setLatitud(Double.parseDouble(ll[0]));
				u.setLongitud(Math.abs(Double.parseDouble(ll[1])));

				VOTeatro te= new VOTeatro();
				te.setFranquicia(franquicias.get(franquicia));
				te.setNombre(nombre);
				te.setUbicacion(u);
				grafoDistancia.agregarVertice(te);
				grafoTiempo.agregarVertice(te);
//				System.out.println("Nombre teatro: "+nombre+"\n  UbicaciÃ³n geogrÃ¡fica: "+ubicacion+"\n   Franquicia: "+franquicia);

			}
		} 
	}

	private static void handleObjectC(JsonReader reader) throws NumberFormatException, Exception
	{
		reader.beginArray();
		while (reader.hasNext()) {

			JsonToken token = reader.peek();
			if (token.equals(JsonToken.BEGIN_OBJECT)) //
				handleArrayC(reader);
			else if (token.equals(JsonToken.END_OBJECT)) {
				reader.endObject();
				DIA=0;
				return;
			}
		}
	}

	private static void handleArrayC(JsonReader reader) throws NumberFormatException, Exception
	{		
		String teatro="";
		String teatroTemp="";
		reader.beginObject();
		RedBlackBST<Integer, RedBlackBST<Integer, VOPeliculaPlan>> funciones= new RedBlackBST();
		while (true) {

			JsonToken token = reader.peek();
			if(token.equals(JsonToken.END_OBJECT))
				reader.endObject();
			if (token.equals(JsonToken.END_ARRAY)) {
				return;
			} else if (token.equals(JsonToken.BEGIN_OBJECT)) {
				handleArrayC(reader);
			} 

			else if(token.equals(JsonToken.NAME)) 
			{

				String res= reader.nextName();
				if(res.equals("teatros"))
				{
					teatroTemp= reader.nextString();
					System.out.println("---------------- Nombre teatro: "+teatroTemp);
				}
				else if(res.equals("teatro"))
				{
					reader.beginObject();
				}
				else if(res.equals("peliculas"))
				{
					String id="";
					String idTemp="";
					RedBlackBST<Integer, VOPeliculaPlan> arbolPel= new RedBlackBST<>();
					VOPeliculaPlan plan;
					while(true)
					{
						JsonToken token3= reader.peek();
						plan=new VOPeliculaPlan();
						if(token3.equals(JsonToken.BEGIN_ARRAY))
						{
							reader.beginArray();
						}
						else if(token3.equals(JsonToken.END_OBJECT))
						{
							reader.endObject();
						}
						else if (token3.equals(JsonToken.END_ARRAY)) {
							reader.endArray();
							reader.endObject();
							reader.endObject();
							break;
						} else if (token3.equals(JsonToken.BEGIN_OBJECT)) {
							reader.beginObject();
						} 
						else if(token3.equals(JsonToken.NAME)) 
						{

							String res0= reader.nextName();
							if(res0.equals("id"))
							{
								idTemp= reader.nextString();
								//								System.out.println("Id Pelicula: "+id);
							}
							else if(res0.equals("funciones"))
							{
								reader.beginArray();
								while(true)
								{
									String hora="x";
									JsonToken token2=reader.peek();

									if(token2.equals(JsonToken.END_OBJECT))
									{
										reader.endObject();
									}
									else if (token2.equals(JsonToken.END_ARRAY)) {
										reader.endArray();
										reader.endObject();
										break;
									} else if (token2.equals(JsonToken.BEGIN_OBJECT)) {
										reader.beginObject();
									} 
									else if(token2.equals(JsonToken.NAME)) 
									{
										String res1= reader.nextName();
										if(res1.equals("hora"))
										{
											hora= reader.nextString();
											plan.setDia(DIA);
											String[] f= hora.split(":");
											String [] h= f[1].split("\\ ");
											int hr= Integer.parseInt(f[0]);
											int min= Integer.parseInt(h[0]);

											if(hr != 12 && h[1].equals("p.m."))
											{
												hr=hr+12;
											}
											Time t=new Time(hr, min, 0);
											plan.setHoraInicio(t);
											Time tf= new Time(hr+2, min, 0);
											plan.setHoraFin(tf);
											System.out.println(teatroTemp);
											if(grafoTiempo.darVertice(teatroTemp).getFunciones().get(DIA)!= null)
											{
												grafoTiempo.darVertice(teatroTemp).getFunciones().get(DIA).put(Integer.parseInt(idTemp), plan);
											}
											else{
												grafoTiempo.darVertice(teatroTemp).getFunciones().put(DIA, new RedBlackBST<Integer, VOPeliculaPlan>());
												grafoTiempo.darVertice(teatroTemp).getFunciones().get(DIA).put(Integer.parseInt(idTemp), plan);
											}

											reader.endObject();
										}
									}
								}

							}			
						}
						if(!idTemp.equals(id))
						{
							id=idTemp;
							//							funciones.put(Integer.parseInt(id), arbolPel);
							//							arbolPel= new RedBlackBST<>();
							//							System.out.println("Id de la pelicula: "+id);
						}

					}
				}
			}
			else{
				reader.skipValue();
			}

			if(!teatroTemp.equals(teatro))
			{
				funciones= new RedBlackBST<>();
				teatro=teatroTemp;
			}
		} 
	}


	private static void handleObjectR(JsonReader reader) throws Exception
	{
		reader.beginArray();
		while (reader.hasNext()) {

			JsonToken token = reader.peek();
			if (token.equals(JsonToken.BEGIN_OBJECT)) //
				handleArrayR(reader);
			else if (token.equals(JsonToken.END_OBJECT)) {
				reader.endObject();
				return;
			}
		}
	}

	private static void handleArrayR(JsonReader reader) throws Exception
	{		

		String teatro1= "";
		String teatro2="";
		String tiempo="";
		String disTemp="";
		reader.beginObject();
		while (true) {

			JsonToken token = reader.peek();
			if(token.equals(JsonToken.END_OBJECT))
			{
				reader.endObject();
			}
			else if (token.equals(JsonToken.END_ARRAY)) {
				return;
			} else if (token.equals(JsonToken.BEGIN_OBJECT)) {
				handleArrayR(reader);
			} 
			else if(token.equals(JsonToken.NAME)) 
			{

				String res= reader.nextName();
				if(res.equals("Teatro 1"))
				{
					teatro1= reader.nextString();
				}
				else if(res.equals("Teatro 2"))
				{
					teatro2= reader.nextString();
				}
				else if(res.equals("Tiempo (minutos)"))
				{
					disTemp= reader.nextString();
				}
			}
			else{
				reader.skipValue();
			}

			if(!disTemp.equals(tiempo))
			{
				tiempo=disTemp;
				VOArco ar= new VOArco(Integer.parseInt(tiempo));
				grafoTiempo.agregarArco(teatro1, teatro2, ar);
			}
		} 
	}

	private static void handleObjectM(JsonReader reader) throws Exception
	{
		reader.beginArray();
		while (reader.hasNext()) {

			JsonToken token = reader.peek();
			if (token.equals(JsonToken.BEGIN_OBJECT)) //
				handleArrayM(reader);
			else if (token.equals(JsonToken.END_OBJECT)) {
				reader.endObject();
				return;
			}
		}
	}

	private static void handleArrayM(JsonReader reader) throws Exception
	{		

		String id= "";
		String nombre="";
		String genero="";
		boolean a=false;
		reader.beginObject();
		while (true) {

			JsonToken token = reader.peek();
			if(token.equals(JsonToken.END_OBJECT))
			{
				reader.endObject();
			}
			else if (token.equals(JsonToken.END_ARRAY)) {
				return;
			} else if (token.equals(JsonToken.BEGIN_OBJECT)) {
				handleArrayM(reader);
			} 
			else if(token.equals(JsonToken.NAME)) 
			{

				String res= reader.nextName();
				if(res.equals("movie_id"))
				{
					id= reader.nextString();
				}
				else if(res.equals("title"))
				{
					nombre= reader.nextString();
				}
				else if(res.equals("genres"))
				{
					genero= reader.nextString();
					a=true;
				}
			}
			else{
				reader.skipValue();
			}

			if(a)
			{
				a=false;

				System.out.println(id+"    "+nombre+"      "+genero);
				String[] generos= genero.split("\\|");
				VOPelicula p= new VOPelicula(Integer.parseInt(id), nombre, generos);
				peliculas.put(Integer.parseInt(id), p);

			}
		} 
	}

	private static void handleObjectS(JsonReader reader) throws Exception
	{
		reader.beginArray();
		while (reader.hasNext()) {

			JsonToken token = reader.peek();
			if (token.equals(JsonToken.BEGIN_OBJECT)) //
				handleArrayS(reader);
			else if (token.equals(JsonToken.END_OBJECT)) {
				reader.endObject();
				return;
			}
		}
	}

	private static void handleArrayS(JsonReader reader) throws Exception
	{		

		String pelicula= "";
		String idTemp="";
		String similitud="";

		MaxPQ<VOSimilitud> heap= new MaxPQ<>();
		boolean a=false;
		reader.beginObject();
		while (true) {

			JsonToken token = reader.peek();
			if(token.equals(JsonToken.END_OBJECT))
			{
				reader.endObject();
			}
			else if (token.equals(JsonToken.END_ARRAY)) {
				return;
			} else if (token.equals(JsonToken.BEGIN_OBJECT)) {
				handleArrayS(reader);
			} 
			else if(token.equals(JsonToken.NAME)) 
			{

				String res= reader.nextName();
				if(res.equals("movieId"))
				{
					a=true;
					pelicula= reader.nextString();
				}
				else
				{
					String x= reader.nextString();
					idTemp= res;
					if(x.equals("NaN"))
					{
						similitud="0";
					}
					else{
						similitud=x;
					}
					VOSimilitud sim= new VOSimilitud(Double.parseDouble(similitud), Integer.parseInt(idTemp));
					heap.insert(sim);
				}
			}
			else{
				reader.skipValue();
			}

			if(a)
			{
				a=false;
				VOPelicula p=peliculas.get(Integer.parseInt(pelicula));
				p.setSimilitudes(heap);
			}
		} 
	}

	public RedBlackBST<Integer, VOPelicula> darPeliculas()
	{
		return peliculas;
	}

	public GrafoDirigido<String, VOTeatro, VOArco> darGrafoTiempo()
	{
		return grafoTiempo;
	}

	public SeparateChainingHashST<String, VOFranquicia> darFranquicias()
	{
		return franquicias;
	}

	public GrafoDirigido<String, VOTeatro, VOArco> darGrafoDistancia()
	{
		return grafoDistancia;
	}
	
	public boolean cargarRatingsSR(String rutaRaitings) {
		String csv = rutaRaitings;
		BufferedReader br = null;
		String linea = "";
		String separador = ",";

		try {
			br = new BufferedReader(new FileReader(csv));

			linea = br.readLine();
			int y=1;
			while ((linea = br.readLine()) != null ) {

				String[] doc = linea.split(separador);

				String user = doc[0];
				String movie = doc[1];
				String rating = doc[2];
				
				VOPelicula p=peliculas.get(Integer.parseInt(movie));
				
				if(usuarios.isEmpty())
				{
					VOUsuario usuario= new VOUsuario();
					usuario.setId(Integer.parseInt(user));

					if(p != null)
					{
						usuario.getPeliculas().put(p.getId(), p);
					}
					usuarios.put(usuario.getId(),usuario);
				}
				else
				{
					VOUsuario u = usuarios.get(Integer.parseInt(user));

					if(u == null)
					{
						u= new VOUsuario();
						u.setId(Integer.parseInt(user));
						if(p!= null)
						{
							u.getPeliculas().put(p.getId(), p);
						}
						usuarios.put(Integer.parseInt(user),u);
					}
					else
					{
						if(p!=null)
						{
						u.getPeliculas().put(p.getId(), p);;
						}
					usuarios.put(u.getId(), u);
					}
				}
				
				y++;
			}


//			Iterator<Integer> it = usuarios.keys().iterator();

//			while(it.hasNext())
//			{

//				VOUsuario usuario = usuarios.get(it.next());
//
//				Iterator<Integer> it2 = usuariosPeliculas.keys().iterator();
//				while(it2.hasNext())
//				{
//					Integer i= it2.next();
//					if(i==usuario.getIdUsuario())
//					{
//						ILista<VOUsuarioPelicula> lista= usuariosPeliculas.get(i);
//						for (int j = 0; j < lista.darTamanio(); j++) 
//						{
//							VOUsuarioPelicula usuarioPelicula= lista.get(j);
//							//							System.out.println("ID USUARIO PELICULA "+usuarioPelicula.getIdPelicula()+"----------------- "+usuario.getIdUsuario());
//
//							usuario.getPeliculasCalificadas().put(usuarioPelicula.getIdPelicula(), usuarioPelicula);
//
//						}
//					}
//				}
//				System.out.println("Cargando...");
//			}

			//			//pruebas
			//			System.out.println("Numero de usuarios pelicula: "+usuariosPeliculas.size());
			//			System.out.println("Id de la pelicula numero 4: "+usuariosPeliculas.get(3).getIdPelicula());
			//			System.out.println("Nombre de la pelicula numero 4: "+usuariosPeliculas.get(3).getNombrepelicula());
			//			System.out.println("Id del usuario de pelicula numero 4: "+usuariosPeliculas.get(3).getIdUsuario());
			//			System.out.println("Rating del usuario de la pelicula numero 4: "+usuariosPeliculas.get(3).getRatingUsuario());
			//			System.out.println("Rating del sistema de la pelicula numero 4: "+usuariosPeliculas.get(3).getRatingSistema());
			//			System.out.println("Error rating de la pelicula numero 4: "+usuariosPeliculas.get(3).getErrorRating());
			//			System.out.println("Timestamp de la pelicula numero 4: "+usuariosPeliculas.get(3).getTimestamp());

			//			VOUsuario u = usuarios.get(547);
			//
			//			System.out.println("-------------------------------------------------------------");
			//			System.out.println("Numero de usuarios: "+usuarios.size());
			//			System.out.println("Id del usuario numero 547: "+u.getIdUsuario());
			//			System.out.println("No. Ratings del usuario numero 547: "+u.getNumRatings());
			//			System.out.println("No. Tags del usuario numero 547: "+u.getNumTags());
			//			System.out.println("Primer timestamp usuario numero 547: "+u.getPrimerTimestamp());
			//			System.out.println("No. de peliculas calificadas del usuario numero 547: "+u.getPeliculasCalificadas().size());
			return true;
		} 
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
			return false;
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
			return false;
		} 
		finally 
		{
			if (br != null) 
			{
				try 
				{
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
					return false;
				}
			}
		}
	}
}
